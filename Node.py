class Node :
    def __init__(self, val) :
        self.val = val
        self.level = 0
        self.left = None
        self.right = None
        self.length = None
        self.parent = None
        
        
    def is_leaf(self):
        result = False
        if self.right==None and self.left==None:
            result = True
        return result
    
    def is_child(self):
        return self.parent != None
    
    def is_father(self):
        return self.right != None or self.left != None
    
    def reset(self):
        self.left=None
        self.right=None
        return
    
    def str(self):
        return str(self.val)
    
    def represent(self, level=0):
        ret = "\t"*level+represent(self.val)+"\n"
        if self.left : 
            ret += self.left.represent(level+1)
        if self.right :
            ret += self.right.represent(level+1)
        return ret
    
    
        