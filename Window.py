import tkinter as tk
import customtkinter as ctk
import os
import sys
import logging
import datetime

class Window :
    def __init__(self):
        
        ctk.set_appearance_mode("dark")  # Modes: system (default), light, dark
        ctk.set_default_color_theme("green")  # Themes: blue (default), dark-blue, green

        self.root = ctk.CTk()  # create CTk window like you do with the Tk window
        
        self.width = 400
        self.height = 650
        
        self.root.geometry("{}x{}".format(self.width, self.height))
        self.root.resizable(False, False)
        self.root.title("Tree Viewer")

        self.root.iconbitmap("./asset/white-app-icon.ico")
        
        button = ctk.CTkButton(master=self.root, text="Generate tree", command=self.button_function)
        button.place(relx=0.5, rely=0.5, anchor=tk.S)

        self.root.mainloop()
        
    def button_function(self):
        print("Génération de l'arbre")