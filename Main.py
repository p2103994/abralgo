import random
from ABR import *
from Node import *
from Window import *

def appendValues():
    with open ("tree.csv", "r") as f:
        line = f.readline()
        line = line.split(",")
        result = []
        for i in line :
               result.append(Node(int(i)))
        return result
    
if __name__ == "__main__":
    window = Window()