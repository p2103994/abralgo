import time

class ABR:
    def __init__(self, values):
        self.values = values
        self.root = self.values[0]
        self.root.level = 0;
        self.root.parent = None
        self.CurrentNode = self.root
        self.buildTree()
        self.minNode = self.min()
        self.maxNode = self.max()

    def buildTree(self):
        for i in range(1, len(self.values)-1):
            self.plantNode(self.values[i])

    def minValue(self):
        return

    def sortValue(self):
        n = len(self.values)
        for i in range(0,n):
            j=i
            while(j>0 and self.values[j-1].val > self.values[j].val):
                self.values[j-1], self.values[j] = self.values[j],  self.values[j-1]
                j=j-1

    def plantBalanceNode(self, node):
        self.values.append(node)
        self.sortValue()
        self.plantDicho(0,len(self.values))
        #ajout dichotomique

    def plantDicho(self, debut, fin):
        time.sleep(0.5)
        if debut>=fin:
            print(None)
            return 
        else :
            mid = (fin-debut)//2
            print(debut, mid , fin)
            print(self.values[mid].val)
            if debut <= mid-1:
                self.plantDicho(debut,mid-1)
            if mid+1 <= fin:
                self.plantDicho(mid+1,fin)  


    def plantNode(self, node):
        currentNode = self.root
        print("planting node : " + str(node.val))
        self.plant(node, currentNode);
           
    def plant(self, node, currentNode):
        print("currentNode : " + str(currentNode.val))
        if currentNode.is_leaf():
            node.parent = currentNode
            node.level = currentNode.level + 1
            if currentNode.val >= node.val:
                print("planting right " + currentNode.str() + "->" + node.str())
                currentNode.right = node
            elif currentNode.val < node.val:
                print("planting left " + currentNode.str() + "->" + node.str()) 
                currentNode.left = node
        else:
            if currentNode.val >= node.val:
                self.plant(node, currentNode.right)
            elif currentNode.val < node.val:
                self.plant(node, currentNode.left)
        return
    def searchNode(self, node):
        return self.search(node, self.root)

    def search(self, value, currentNode):
        currentNodeVal = currentNode.val
        if currentNode.is_leaf():
            return None
        else:
            if currentNode.val == value:
                return currentNode;
            elif currentNode.val >= value:
                return self.search(value, currentNode.right)
            elif currentNode.val < value:
                return self.search(value, currentNode.left)

    # Explorer

    def browseInfinxe(self, currentNode):
        if currentNode.right!=None: 
            self.browseInfinxe(currentNode.right)
        print(currentNode.val)
        if currentNode.left!=None: 
            self.browseInfinxe(currentNode.left)

    def browseInfinxeaddTable(self, currentNode, tab):
        if currentNode.right!=None: 
            self.browseInfinxeaddTable(currentNode.right, tab)
        tab.append(currentNode.val)
        if currentNode.left!=None: 
            self.browseInfinxeaddTable(currentNode.left, tab)
        return(tab)

    def min(self):
        currentNode=self.root
        while currentNode.right != None:
            currentNode=currentNode.right
        return currentNode

    def max(self):
        currentNode=self.root
        while currentNode.left != None:
            currentNode=currentNode.left
        return currentNode

    def sortArray(tab):
        arbre=ABR(tab)
        return(arbre.browseInfinxeaddTable(arbre.root,[]))
    
    def delete(self, value):
        node = self.search(value, self.root);
        if node is None :
            return False
        else :
            parentNode = node.parent;
            if node.val >= parentNode.val:
                parentNode.left = None
            elif node.val < parentNode.val:
                parentNode.right = None
            return True
